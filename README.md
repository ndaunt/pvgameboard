# _PVGAMEBOARD_

#### _Bink, an interview in the form of a multi-platform video game October 2017_

#### By _**Nico Daunt**_

## Description

_I spent the last 4 days  with Unity, and C# to provide you with this experience. Bink is a 6x6 virtual tabletop video game featuring Bink, our very own purple alien superhero protagonist._

## Setup/Installation Requirements
#### Windows:
* _Run Bink.exe_
* _Scroll through 3 different equippable items by pressing numbers 1, 2, 3_
* _Pressing 4 drops any item equipped_
* _Don't walk off the edge! Just kidding, invisible boundaries will prevent that from happening_
* _Take in the sweet sounds of Don Gero's chip-tunes, sampled from Gameboys with hacked circuitry._
* _Play through Unity in scene:WORKS2 for better results!

#### IOS or ANDROID:
* _Download unityRemote5 from app store_
* _Connect Phone or Touchpad device to Computer through USB_
* _Unzip MobileProject file_
* _Open PVGAMEMOBILEFINAL_
* _Navigate to EDIT> Project Settings> Editor_
* _In right panel select mobile device from drop down menu._
* _Set Resolution to that of testing device under the  Game Tab, and press play!_


## Known Bugs

_Item Selection in mobile mode is prone to glitching, or not working at all. Project files could use some future organization, but I had deadlines to stick to! BinkWindows has movement glitches, most likely due to missing components in Bink.exe/ Bink_Data, try playing through scene Works 2 in FULL PROJECT file for best results._

## Support and contact details

_Nico Daunt, nico@humanfleshbody.word (415)595-0819_

## Technologies Used/ Special Thanks

_Made with C# and Unity 5.6.3.p1, GameJam UI template for audio scripting/ menu items, Background Music is Don Gero Tune from "The Dawn of Gero, Early Tracks/Unreleased"_


 2017 **_{Nico Daunt}_**
